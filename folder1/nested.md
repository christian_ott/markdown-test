# A Nested Markdown file

It includes an image from the root folder:

![](../root-level-small.jpg)

An image from the current folder:

![](nested-small.jpg)


And an image from a deeply nested folder:

![](../folder2/deep/deep/nested/folder/deeply-nested-small.jpg)

